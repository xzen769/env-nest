import { Injectable } from '@nestjs/common';
import { Task } from './task.models';

@Injectable()
export class TasksService {
  public tasks:Task[];

  constructor() {
    this.tasks = [{
      id: '1',
      task: 'acheter du lait',
      date: new Date(),
      done: false
    }, {
      id: '2',
      task: 'aller au sport',
      date: new Date(),
      done: false
    }]
  }

  getTasks(): Task[] {
    return this.tasks
  }

  getTask(id:string): Task | Object {
    const task = this.tasks.filter((task) => task.id === id)
  
    return !task.length ? {} : task[0]
  }

  checkTask(id:string): Task | Object {
    const task:Task = this.tasks.find((task, index) => {
      task.done = !task.done;

      return task.id === id;
    });

    if (!task)
      return {};

    task.done = !task.done;

    return task;
  }
}
