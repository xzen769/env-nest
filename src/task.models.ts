export interface Task {
  id: string,
  task: string,
  date: Date,
  done: boolean
};