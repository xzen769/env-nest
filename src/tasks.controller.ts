import { Controller, Get, Param } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { Task } from './task.models';

@Controller('tasks')
export class TasksController {
  constructor(private readonly taskService: TasksService) {}

  @Get()
  getTasks(): Task[] {
    return this.taskService.getTasks();
  }

  @Get(':id')
  getTask(@Param('id') id:string): Task | Object {
    return this.taskService.getTask(id);
  }

  @Get(':id/check')
  checkTask(@Param('id') id:string): Task | Object {
    return this.taskService.checkTask(id);
  }
}
